﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsAPI
{
    public partial class FormHome : Form
    {
        public FormHome()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            form1.FormClosed += (s, args) => this.Close(); // Close FormLogin when Form1 is closed
            form1.Show();
            this.Hide(); // Hide the current form
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FormDownload form1 = new FormDownload();
            form1.FormClosed += (s, args) => this.Close(); // Close FormLogin when Form1 is closed
            form1.Show();
            this.Hide(); // Hide the current form
        }
    }
}
