﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;

namespace WindowsFormsAPI
{
    public partial class FormAccount : Form
    {
        public FormAccount()
        {
            InitializeComponent();
        }

        private void FormAccount_Load(object sender, EventArgs e)
        {
            loadData();
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            InsertData(txtLink.Text, txtUserName.Text, txtPassword.Text, txtName.Text, txtNotes.Text);
            loadData();
            clearTextbox();
        }

        private void InsertData(string link, string userName, string password, string name, string notes)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(GlobalConfig.ConnectionString))
                {
                    connection.Open();
                    string query = "INSERT INTO [dbo].[DataAccount] (Link, Username, Password, CreatedDate, Name, Notes) VALUES (@Link, @UserName, @Password, @CreatedDate, @Name, @Notes)";
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Link", link);
                        command.Parameters.AddWithValue("@UserName", userName);
                        command.Parameters.AddWithValue("@Password", password);
                        command.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
                        command.Parameters.AddWithValue("@Name", name);
                        command.Parameters.AddWithValue("@Notes", notes);

                        command.ExecuteNonQuery();
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Terjadi kesalahan: " + ex.Message);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            UpdateData(int.Parse(txtUserId.Text), txtLink.Text, txtUserName.Text, txtPassword.Text, txtName.Text, txtNotes.Text);
            loadData();
            clearTextbox();
        }

        private void UpdateData(int userId, string displayName, string userName, string password, string Name, string Notes)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(GlobalConfig.ConnectionString))
                {
                    connection.Open();
                    string query = "UPDATE [dbo].[DataAccount] SET Link = @Link, UserName = @UserName, Password = @Password, Name = @Name, Notes = @Notes WHERE Id = @Id";
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Id", userId);
                        command.Parameters.AddWithValue("@Link", displayName);
                        command.Parameters.AddWithValue("@UserName", userName);
                        command.Parameters.AddWithValue("@Password", password);
                        command.Parameters.AddWithValue("@Name", Name);
                        command.Parameters.AddWithValue("@Notes", Notes);

                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Terjadi kesalahan: " + ex.Message);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteData(int.Parse(txtUserId.Text));
            loadData();
            clearTextbox();
        }

        private void DeleteData(int userId)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(GlobalConfig.ConnectionString))
                {
                    connection.Open();
                    string query = "DELETE FROM [dbo].[DataAccount] WHERE Id = @Id";
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Id", userId);
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Terjadi kesalahan: " + ex.Message);
            }
        }

        private void loadData()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(GlobalConfig.ConnectionString))
                {
                    connection.Open();
                    string query = "SELECT * FROM [dbo].[DataAccount]";

                    SqlDataAdapter dataAdapter = new SqlDataAdapter(query, connection);
                    DataTable dataTable = new DataTable();
                    dataAdapter.Fill(dataTable);

                    dataGridView1.DataSource = dataTable;
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Terjadi kesalahan: " + ex.Message);
            }
        }

        private void dataGridView1_SelectionChanged_1(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                DataGridViewRow row = dataGridView1.SelectedRows[0];
                txtUserId.Text = row.Cells["Id"].Value.ToString();
                txtName.Text = row.Cells["Name"].Value.ToString();
                txtLink.Text = row.Cells["Link"].Value.ToString();
                txtUserName.Text = row.Cells["Username"].Value.ToString();
                txtPassword.Text = row.Cells["Password"].Value.ToString();
                txtNotes.Text = row.Cells["Notes"].Value.ToString();
            }
        }

        private void clearTextbox()
        {
            txtUserId.Text = "";
            txtLink.Text = "";
            txtUserName.Text = "";
            txtPassword.Text = "";
            txtName.Text = "";
            txtNotes.Text = "";
        }

        private void dataGridView1_SelectionChanged_1(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                DataGridViewRow row = dataGridView1.SelectedRows[0];
                txtUserId.Text = row.Cells["Id"].Value.ToString();
                txtLink.Text = row.Cells["Name"].Value.ToString();
                txtLink.Text = row.Cells["Link"].Value.ToString();
                txtUserName.Text = row.Cells["Username"].Value.ToString();
                txtPassword.Text = row.Cells["Password"].Value.ToString();
                txtLink.Text = row.Cells["Notes"].Value.ToString();
            }
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }
    }
}
