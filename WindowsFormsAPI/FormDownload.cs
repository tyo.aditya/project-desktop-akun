﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using VideoLibrary;

namespace WindowsFormsAPI
{
    public partial class FormDownload : Form
    {
        public FormDownload()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string url = TxtLinkDownload.Text;
            var youtube = YouTube.Default;
            try
            {
                var video = youtube.GetVideo(url);
                string filepath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), video.FullName);
                File.WriteAllBytes(filepath, video.GetBytes());
                MessageBox.Show("Video downloaded successfully to: " + filepath);
            }
            catch (Exception)
            {
                MessageBox.Show("Error Download");
            }
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            string url = TxtLinkDownload.Text;
            var youtube = YouTube.Default;
            try
            {
                ////https://www.youtube.com/watch?v=s-MsZo02dos
                var video = youtube.GetVideo(url);
                string downloadsPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), "Downloads");

                // Combine the Downloads path with the video's file name
                string filepath = Path.Combine(downloadsPath, video.FullName);

                // Write the video bytes to the file
                File.WriteAllBytes(filepath, video.GetBytes());

                // Notify the user that the video was downloaded successfully
                MessageBox.Show("Video downloaded successfully to: " + filepath);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Download : "+ex.Message);
            }
        }
    }
}
