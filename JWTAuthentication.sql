/*
 Navicat Premium Data Transfer

 Source Server         : sqlserverlokal
 Source Server Type    : SQL Server
 Source Server Version : 16001000 (16.00.1000)
 Source Host           : LOCALHOST:1433
 Source Catalog        : JWTAuthentication
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 16001000 (16.00.1000)
 File Encoding         : 65001

 Date: 06/06/2024 17:13:34
*/


-- ----------------------------
-- Table structure for Account
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Account]') AND type IN ('U'))
	DROP TABLE [dbo].[Account]
GO

CREATE TABLE [dbo].[Account] (
  [Id] int  NOT NULL,
  [Link] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Username] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Password] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [CreatedDate] datetime2(7)  NULL
)
GO

ALTER TABLE [dbo].[Account] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of Account
-- ----------------------------
INSERT INTO [dbo].[Account] ([Id], [Link], [Username], [Password], [CreatedDate]) VALUES (N'1', N'https://portal.stagingapps.net/login', N'prasetyo.aditya@wgs.co.id', N'1707017', NULL)
GO


-- ----------------------------
-- Table structure for Employee
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Employee]') AND type IN ('U'))
	DROP TABLE [dbo].[Employee]
GO

CREATE TABLE [dbo].[Employee] (
  [EmployeeID] int  NOT NULL,
  [NationalIDNumber] nvarchar(15) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [EmployeeName] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [LoginID] nvarchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [JobTitle] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [BirthDate] date  NOT NULL,
  [MaritalStatus] nchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Gender] nchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [HireDate] date  NOT NULL,
  [VacationHours] smallint  NOT NULL,
  [SickLeaveHours] smallint  NOT NULL,
  [rowguid] uniqueidentifier  NOT NULL ROWGUIDCOL,
  [ModifiedDate] datetime  NOT NULL
)
GO

ALTER TABLE [dbo].[Employee] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of Employee
-- ----------------------------
INSERT INTO [dbo].[Employee] ([EmployeeID], [NationalIDNumber], [EmployeeName], [LoginID], [JobTitle], [BirthDate], [MaritalStatus], [Gender], [HireDate], [VacationHours], [SickLeaveHours], [rowguid], [ModifiedDate]) VALUES (N'1', N'295847284', N'Michael Westover', N'adventure-works\ken0', N'Vice President of Sales', N'1969-01-29', N'S', N'M', N'2009-01-14', N'99', N'69', N'F01251E5-96A3-448D-981E-0F99D789110D', N'2014-06-30 00:00:00.000')
GO

INSERT INTO [dbo].[Employee] ([EmployeeID], [NationalIDNumber], [EmployeeName], [LoginID], [JobTitle], [BirthDate], [MaritalStatus], [Gender], [HireDate], [VacationHours], [SickLeaveHours], [rowguid], [ModifiedDate]) VALUES (N'2', N'245797967', N'Raeann Santos', N'adventure-works\terri0', N'Vice President of Engineering', N'1971-08-01', N'S', N'F', N'2008-01-31', N'1', N'20', N'45E8F437-670D-4409-93CB-F9424A40D6EE', N'2014-06-30 00:00:00.000')
GO

INSERT INTO [dbo].[Employee] ([EmployeeID], [NationalIDNumber], [EmployeeName], [LoginID], [JobTitle], [BirthDate], [MaritalStatus], [Gender], [HireDate], [VacationHours], [SickLeaveHours], [rowguid], [ModifiedDate]) VALUES (N'3', N'509647174', N'Pamela Wambsgans', N'adventure-works\roberto0', N'Engineering Manager', N'1974-11-12', N'M', N'M', N'2007-11-11', N'2', N'21', N'9BBBFB2C-EFBB-4217-9AB7-F97689328841', N'2014-06-30 00:00:00.000')
GO

INSERT INTO [dbo].[Employee] ([EmployeeID], [NationalIDNumber], [EmployeeName], [LoginID], [JobTitle], [BirthDate], [MaritalStatus], [Gender], [HireDate], [VacationHours], [SickLeaveHours], [rowguid], [ModifiedDate]) VALUES (N'4', N'509647174', N'Pamela Wambsgans test', N'adventure-works\roberto0', N'Engineering Manager', N'1974-11-12', N'M', N'M', N'2007-11-11', N'2', N'21', N'9BBBFB2C-EFBB-4217-9AB7-F97689328841', N'2014-06-30 00:00:00.000')
GO


-- ----------------------------
-- Table structure for UserInfo
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[UserInfo]') AND type IN ('U'))
	DROP TABLE [dbo].[UserInfo]
GO

CREATE TABLE [dbo].[UserInfo] (
  [UserId] int  IDENTITY(1,1) NOT NULL,
  [DisplayName] varchar(60) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [UserName] varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Email] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Password] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [CreatedDate] datetime  NOT NULL
)
GO

ALTER TABLE [dbo].[UserInfo] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of UserInfo
-- ----------------------------
SET IDENTITY_INSERT [dbo].[UserInfo] ON
GO

INSERT INTO [dbo].[UserInfo] ([UserId], [DisplayName], [UserName], [Email], [Password], [CreatedDate]) VALUES (N'1', N'Amit Mohanty', N'Admin', N'admin@abc.com', N'$admin@2022', N'2022-01-17 14:47:58.207')
GO

INSERT INTO [dbo].[UserInfo] ([UserId], [DisplayName], [UserName], [Email], [Password], [CreatedDate]) VALUES (N'2', N'tyo ', N'tyo ', N'tyo@mail.com', N'12345', N'2024-01-24 09:20:06.913')
GO

INSERT INTO [dbo].[UserInfo] ([UserId], [DisplayName], [UserName], [Email], [Password], [CreatedDate]) VALUES (N'5', N'calvin ', N'calvin', N'calvin@mail.com', N'12345', N'2024-06-06 11:12:09.090')
GO

SET IDENTITY_INSERT [dbo].[UserInfo] OFF
GO


-- ----------------------------
-- Primary Key structure for table Account
-- ----------------------------
ALTER TABLE [dbo].[Account] ADD CONSTRAINT [PK__Account__3214EC076EC1F320] PRIMARY KEY CLUSTERED ([Id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table Employee
-- ----------------------------
ALTER TABLE [dbo].[Employee] ADD CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED ([EmployeeID])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for UserInfo
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[UserInfo]', RESEED, 6)
GO


-- ----------------------------
-- Primary Key structure for table UserInfo
-- ----------------------------
ALTER TABLE [dbo].[UserInfo] ADD CONSTRAINT [PK_UserInfo] PRIMARY KEY CLUSTERED ([UserId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO

